
#%% Import libraries
import pickle
import matplotlib.pyplot as plt
import numpy as np
#%% Import from file
from print_series import get_signals_array,df_series,df_start_stop
#%%
df_series_all = df_series()
df_start_stop_all = df_start_stop()
print(df_series_all.head)
print(df_start_stop_all.head)
#%%
signal_array = get_signals_array(df_start_stop_all,df_series_all,machine_name='LHL_05')
print(len(signal_array))

#%% Works
for i in range(0,(len(signal_array)-1)//30):
    plt.plot(signal_array[i])
plt.show()

#%%
plt.plot(np.sort([np.median(sig) for sig in signal_array]))
plt.show()

#%%
np.sort([np.median(sig) for sig in signal_array])[175]

#%% plot the signals with median = 0
for i in range(0,(len(signal_array)-1)):
    plt.plot(signal_array[i]-np.median(signal_array[i]))
plt.show()

#%% plot the std
plt.plot([np.std(sig) for sig in signal_array])
plt.show()
#%%
np.sort([np.std(sig) for sig in signal_array])[383]

#%%plot the one that have a small std
for i in range(0,(len(signal_array)-1)//6):
    if np.std(signal_array[i])>8.2:
        plt.plot(signal_array[i]-np.median(signal_array[i]))
plt.show()

#%%plot the duration
plt.plot(np.sort([len(sig) for sig in signal_array ] ))
plt.show()
#%%
np.sort([len(sig) for sig in signal_array ])[100]

#%%plot the one that have a small std and time
for i in range(0,(len(signal_array)-1)//100):
    if np.std(signal_array[i])<8.2 and len(signal_array[i])>100:
        plt.plot(signal_array[i]-np.median(signal_array[i]))
plt.show()


#%% plot the first 50 secs of the sigs 
for i in range(0,(len(signal_array)-1)):
    if np.std(signal_array[i])<8.2 and len(signal_array[i])==105:
        plt.plot(signal_array[i]-np.median(signal_array[i]))
plt.show()

#%% plot the two half into one
for i in range(0,(len(signal_array)-1)):
    if np.std(signal_array[i])<8.2 and len(signal_array[i])==105:
        sample = signal_array[i]-np.median(signal_array[i])
        plt.plot(sample[:53])
        #plt.plot(sample[53:])
plt.show()

#%% plot the two half into one
samples = [sig[:53] for sig in signal_array if np.std(sig)<8.2 and len(sig)==105]
print(len(samples))
print(len(np.transpose(samples)))
ts = np.transpose(samples)
print(type(ts[10]))
#plt.boxplot(ts[10])
plt.show()

#%% plot all signals bigger than 105 but cutting the middle
for i in range(0,(len(signal_array)-1)):
        if np.std(signal_array[i])<8.2 and len(signal_array[i])>105:
                sample = signal_array[i]-np.median(signal_array[i])
                sample105 = np.append(sample[:53],sample[-53:])
                plt.plot(sample105)
plt.show()


#%% plot all signals bigger than 105 but cutting the middle
for i in range(0,(len(signal_array)-1)):
        if np.std(signal_array[i])<8.2 and len(signal_array[i])==115:
                sample = signal_array[i]-np.median(signal_array[i])
                sample105 = np.append(sample[:53],sample[-53:])
                plt.plot(sample)
plt.show()

#%%
#%% save in pickle
with open ('signals5.pickle','wb+') as fp:
        pickle.dump(signal_array,fp)

#%%
