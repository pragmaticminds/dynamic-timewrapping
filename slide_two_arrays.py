
#%%
import pickle
import matplotlib.pyplot as plt
import numpy as np
from scipy.ndimage.interpolation import shift
#%%
def equalize_array_size(array1,array2):
    '''
    reduce the size of one sample to make them equal size. 
    The sides of the biggest signal are truncated

    Args:
        array1 (1d array/list): signal for example the reference
        array2 (1d array/list): signal for example the target

    Returns:
        array1 (1d array/list): middle of the signal if truncated
        array2 (1d array/list): middle of the initial signal if there is a size difference between the array 1 and 2
        dif_length (int): size diffence between the two original arrays 
    '''
    len1, len2 = len(array1), len(array2)
    dif_length = len1-len2
    if dif_length<0:
        array2 = array2[int(np.floor(-dif_length/2)):len2-int(np.ceil(-dif_length/2))]
    elif dif_length>0:
        array1 = array1[int(np.floor(dif_length/2)):len1-int(np.ceil(dif_length/2))]
    return array1,array2, dif_length

#%% 
def slide(reference,query,shift_step=0,order=1):
    if shift_step<0 :
        query, reference  = slide(query,reference,shift_step=-shift_step)
        return reference,query
    big_shift = int(shift_step)
    small_shift = shift_step-int(shift_step)
    if big_shift>0:
        reference = reference[big_shift:]
    #print(shift_step)
    query = shift(query,small_shift,order=order,mode='nearest')
    return reference,query

#%%
def slide_and_equalize(reference,query,shift_step=0,order=1):
    reference, query = slide(reference,query,shift_step=shift_step,order=order)
    dif = len(reference)-len(query)
    if dif>0:
        reference = reference[:len(reference)-dif]
    elif dif<0:
        query = query[:len(query)+dif]
    #plt.plot(query)
    #plt.plot(reference)
    plt.show()
    return reference,query

#%% Import signals
with open('signals5.pickle','rb') as fp:
    signals = pickle.load(fp)

#%%
sig1 = signals[15][53:]
sig2 = signals[11][53:]
plt.plot(sig1)
plt.plot(sig2)
plt.show()
#sig1,sig2,dif = equalize_array_size(sig1,sig2)
#de base, le shift est de 13
#print(dif)
#ref,que = slide(sig1,sig2,13-dif//2)
ref,que = slide_and_equalize(sig1,sig2,shift_step=13.7)
print(len(ref)-len(que))
plt.plot(ref)
plt.plot(que)
plt.show()
#plt.show()

#%%
