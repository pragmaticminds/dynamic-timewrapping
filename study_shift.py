"""
In this file, we want to get informations about
the way signal present a time shift
during cycles
"""

#%% import functions from file
from alignsignals import chisqr_align

#%% import libraries
from scipy.ndimage.interpolation import shift
import numpy as np
import pickle
import matplotlib.pyplot as plt

#%%
with open('signals5.pickle','rb') as fp:
    signal_array = pickle.load(fp)

plt.plot(signal_array[0])
plt.show()

#%% print the data which we want to study the middle shift
for i in range(0,(len(signal_array)-1)):
    if np.std(signal_array[i])<8.2 and len(signal_array[i])>=105 and len(signal_array[i])<115:
        signal_median = signal_array[i]-np.median(signal_array[i])
        plt.plot(signal_median[:53])
plt.show()

#%%
for i in range(0,(len(signal_array)-1)):
    if np.std(signal_array[i])<8.2 and len(signal_array[i])>=118 and len(signal_array[i])<119:
        signal_median = signal_array[i]-np.median(signal_array[i])
        plt.plot(signal_median[:53])
plt.show()

#%%
sig118 = [sig-np.median(sig) for sig in signal_array if len(sig)==118 and np.std(sig)<8.2]

#%%
beginsamples = [sample[:53] for sample in sig118]

#%%
[plt.plot(s) for s in beginsamples[:4]]
plt.show()

#%% The  element 1 of our samples is looking like the other we will take it for comparison
plt.plot(beginsamples[1])
plt.show()
#%%
shifts = [chisqr_align(beginsamples[1],s,bound=26).get('shift') for s in beginsamples]

#%%
plt.boxplot(shifts)

#%%
plt.plot(np.abs(shifts))
plt.plot(np.sort(np.abs(shifts)))
#%%
sig1 = beginsamples[0]
sig2 = beginsamples[1]
plt.plot(sig1)
plt.plot(sig2)
plt.show()
computed_shift = chisqr_align(sig2,sig1,bound=12,order=1).get('shift')
print(computed_shift)
#plt.plot(sig1)
plt.plot(sig2)

plt.plot(shift(sig1,computed_shift,mode='nearest'),ls='--')

plt.show()
#%%
shifted_signals = [shift(s,chisqr_align(beginsamples[1],s,bound=27).get('shift'),mode='nearest') for s in beginsamples]
[plt.plot(s) for s in shifted_signals]
plt.show()

#%%
tr = beginsamples
tr = np.append(tr[0],0)
print(len(tr))
plt.boxplot(tr)

#%%
x = [[1.2, 2.3, 3.0, 4.5],
     [1.1, 2.2, 2.9]]
plt.boxplot(x)
plt.show()

#%%
"""
 about the +105 middle shift because there is actually no 105 middle shift.
 It is like it was a perfect signal
 the problem with 105+ is that some of the signals are already retarded
"""
sig53 = [(sig-np.median(sig))[53:] for sig in signal_array if len(sig)>=105 and np.std(sig)<8.2]

#%%
[plt.plot(s) for s in sig53]
plt.show()
#%%
plt.plot(sig53[2])

#%%
shifts53 = [chisqr_align(sig53[2],sig,bound=20).get('shift') for sig in sig53]

#%%
plt.plot(shifts53)
plt.plot(np.sort(shifts53))

#%%
[plt.plot(sig) for sig in sig53]
plt.show()

#%%
chisqrs_shifts = [chisqr_align(sig53[2],s,bound=15).get('shift') for s in sig53]
shifted_signals = [shift(s,chisqr,mode='nearest') for s,chisqr in zip(sig53,chisqrs_shifts)]
[plt.plot(s) for s in shifted_signals]
plt.show()
plt.boxplot(chisqrs_shifts)
#%%
plt.hist(chisqrs_shifts,bins=50)
#%%study accuracy
error_signals = [chisqr_align(sig53[2],s,bound=15).get('fun') for s in sig53]
plt.plot(np.sort(error_signals))
#%%
