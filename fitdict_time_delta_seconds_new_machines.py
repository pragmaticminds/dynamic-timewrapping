
#%% Import libraries
import pickle
import matplotlib.pyplot as plt
import numpy as np
#%% Machine learning library
from sklearn.mixture import GaussianMixture

#%%import data from pickle
with open('dict_time_delta_seconds_new_machines.pickle','rb') as fp:
    deltas_all = pickle.load(fp)

[print(k) for k,v in deltas_all.items()]
#%%
def plot_gmm(X, model, **kwargs):
    """
    Draw a mixed Gaussian model
    Arguments:
        X -- Sample data
        model -- Mixed Gaussian model to draw
        *kwargs -- `matplotlib.pyplot.bar` additional arguments for 
    Returns:
        x, pdf, pdf_individual: Parameter group used for drawing

    """
    # vector x that supports the plots
    x = np.linspace(np.min(X, axis=0), np.max(X, axis=0), 1000)

    # apply the model on x vector in oder to plot the model prediction
    logprob, responsibilities = model.score_samples(x), model.predict_proba(x)
    pdf = np.exp(logprob)
    pdf_individual = responsibilities * pdf[:,np.newaxis]

    # draw the sum of the individual distributions
    plt.plot(x, pdf, 'r-', **kwargs)

    # draw the individual normal distributions
    args = zip(model.weights_, model.means_, model.covariances_)
    for i, (weight, mean, covar) in enumerate(args):
        # variance (There is no need to calculate separately because it is one-dimensional ... w)
        var = np.diag(covar)[0]
        # 式を作成: N(u, o^2)
        formula_label = "N(%1.2f, %1.2f)" % (mean, var)
        formula_label = "%d%% - %s" % (round(weight * 100), formula_label)
        #print(formula_label)
        
        plt.plot(x, pdf_individual[:,i], 'k--', label=formula_label, alpha=0.5, **kwargs)
    plt.xlabel('$t (sec)$')
    plt.ylabel('$p(t) density$')
    plt.legend()
    
    #return x, pdf, pdf_individual
#%%
def find_best_model(X,n_component_max=5):
    # fit models with 1-10 components
    N = np.arange(1, n_component_max)
    models = [None for i in range(len(N))]

    for i in range(len(N)):
        models[i] = GaussianMixture(N[i]).fit(X)

    # compute the AIC and the BIC
    AIC = [m.aic(X) for m in models]
    BIC = [m.bic(X) for m in models]

    n_component = np.min([np.argmin(AIC),np.argmin(BIC)])
    print('We found ' + str(n_component +1 ) + ' components')
    M_best = models[n_component]

    return M_best

#%% Generate the fitting for all machines
for machine_number,signal in deltas_all.items():
    
    X = np.array(signal).reshape(-1,1) #because it's a single feature
    M_model = find_best_model(X)
    plot_gmm(X, M_model)
    plt.hist(X,100,density=True, label=('machine ' + str(machine_number)))
    plt.legend()
    plt.show()
#%%
#------------------------------------------------------------
# Learn the best-fit GaussianMixture models
#  Here we'll use scikit-learn's GaussianMixture model. The fit() method
#  uses an Expectation-Maximization approach to find the best
#  mixture of Gaussians for the data

# fit models with 1-10 components
signal = deltas_all.get(1)
X = np.array(signal).reshape(-1,1)
N = np.arange(1, 11)
models = [None for i in range(len(N))]

for i in range(len(N)):
    models[i] = GaussianMixture(N[i]).fit(X)

# compute the AIC and the BIC
AIC = [m.aic(X) for m in models]
BIC = [m.bic(X) for m in models]
#%%
#------------------------------------------------------------
# Plot the results
#  We'll use three panels:
#   1) data + best-fit mixture
#   2) AIC and BIC vs number of components
#   3) probability that a point came from each component

fig = plt.figure(figsize=(5, 1.7))
fig.subplots_adjust(left=0.12, right=0.97,
                    bottom=0.21, top=0.9, wspace=0.5)


# plot 1: data + best-fit mixture
ax = fig.add_subplot(111)
#print(np.argmin(AIC))
n_component = np.min([np.argmin(AIC),np.argmin(BIC)])
print('We found ' + str(n_component +1 ) + ' components')
M_best = models[n_component]
#M_best = models[2]

x = np.linspace(55, 70, 1000)
logprob = M_best.score_samples(x.reshape(-1, 1))
responsibilities = M_best.predict_proba(x.reshape(-1, 1))
pdf = np.exp(logprob) #pdf = probability density function
pdf_individual = responsibilities * pdf[:, np.newaxis]

ax.hist(X, 30, density=True, histtype='stepfilled', alpha=0.4)
ax.plot(x, pdf, '-k')
ax.plot(x, pdf_individual, '--k')
ax.text(0.04, 0.96, "Best-fit Mixture",
        ha='left', va='top', transform=ax.transAxes)
ax.set_xlabel('$x$')
ax.set_ylabel('$p(x) density$')
plt.show()


#%%

#%%
# plot 2: AIC and BIC
plt.plot(N, AIC, '-k', label='AIC')
plt.plot(N, BIC, '--k', label='BIC')
plt.legend()
plt.xlabel('n. components')
plt.ylabel('information criterion')

plt.show()

#%%
# plot 3: posterior probabilities for each component
ax = fig.add_subplot(133)

p = responsibilities
p = p[:, (1, 0, 2)]  # rearrange order so the plot looks better
p = p.cumsum(1).T

ax.fill_between(x, 0, p[0], color='gray', alpha=0.3)
ax.fill_between(x, p[0], p[1], color='gray', alpha=0.5)
ax.fill_between(x, p[1], 1, color='gray', alpha=0.7)
ax.set_xlim(-6, 6)
ax.set_ylim(0, 1)
ax.set_xlabel('$x$')
ax.set_ylabel(r'$p({\rm class}|x)$')

ax.text(-5, 0.3, 'class 1', rotation='vertical')
ax.text(0, 0.5, 'class 2', rotation='vertical')
ax.text(3, 0.3, 'class 3', rotation='vertical')

plt.show()

#%%
