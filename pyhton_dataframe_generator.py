
#%% Import Libraries
from scipy import signal, fftpack
import numpy as np
from matplotlib import pyplot as plt
from collections import deque #is optimized for pulling and pushing on both ends. They even have a dedicated rotate() method. 

#%% generate easy samples
a = np.array([0, 1, 2, 3, 4, 3, 2, 1, 0, 1, 2, 3, 4, 3, 2, 1, 0, 0, 0, 0, 0])
b = np.array([0, 0, 0, 0, 0, 1, 2, 3, 4, 3, 2, 1, 0, 1, 2, 3, 4, 3, 2, 1, 0])

#%% generate signal
def generate_signal(length_n = 10,x_set = 0,y_set=0,zoom=0):
    return np.sin(np.arange(x_set,length_n+x_set,)) + y_set 

#%%def generate samples of signal
def generate_samples(n_samples):
    samples = []
    
    for i in range(n_samples):
        x_set = np.random.rand()*3.0
        s = generate_signal(x_set=x_set)
        samples.append(s)

    return samples

#%% our samples
n_samples = 10
samples = generate_samples(n_samples=n_samples)
for i in range(n_samples):
    plt.plot(samples[i])

plt.show()


#%% Vizualise the two first
a = samples[1]
b = samples[2]
plt.plot(a)
plt.plot(b)
plt.show()

#%% Compute time shift
print(len(a))
print(np.argmax(signal.correlate(a,b,mode='same',method='direct')))
shift = np.argmax(signal.correlate(a,b))
np.argmax(signal.correlate(b,a))

#%% Rotate 
#
#value from argmax is off by (signal size -1 ) from what you seem to expect.
print(a)
deque_a = deque(a)
print(deque_a)

print(shift)
deque_a.rotate(len(a)-1-shift)
print(deque_a)

#%% Vizualise
plt.plot(deque_a)
plt.plot(b)
plt.show()

#%%
