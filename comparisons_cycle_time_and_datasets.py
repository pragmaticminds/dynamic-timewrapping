

#%%
import pandas as pd
import pickle
import matplotlib.pyplot as plt
import numpy as np
import json
from scipy.ndimage.interpolation import shift

#%%
def df_start_stop(start_stop_path = 'data\data-1565275276713.csv'):
    """generate dataframe from csv with start stop data
    
    Keyword Arguments:
        start_stop_path {str} -- path to csv file (default: {'/home/pier/Documents/stage/time_series_compare/dynamic-timewrapping/data/data-1565275276713.csv'})
    
    Returns:
        pd.datafram -- df_times
    """
    df_times = pd.read_csv(start_stop_path,delimiter=',')
    #convert to datetime
    df_times['cycle_start_time'] = pd.to_datetime(df_times['cycle_start'])
    df_times['cycle_start_time'] = df_times['cycle_start_time'].dt.tz_localize('Europe/Berlin')
    df_times['cycle_stop_time'] = pd.to_datetime(df_times['cycle_end']).dt.tz_localize('Europe/Berlin')
    df_times.drop(['cycle_start'], axis=1, inplace=True)
    df_times.drop(['cycle_end'], axis=1, inplace=True)

    #set start time as index
    #df_times = df_times.set_index('cycle_start_time')

    return df_times
    
def getdf_start_stop_machine(data,machine_name = 'LHL_06', begintime='2019-07-29',endtime='2019-07-30'):
    """selects the rows of a specific machine (optional) in a specific time frame
    
    Arguments:
        data {pd.dataframe} -- like df_times
    
    Keyword Arguments:
        machine_name {str} --  (default: {'LHL_06'})
        begintime {str} --  (default: {'2019-07-29'})
        endtime {str} --  (default: {'2019-07-30'})
    
    Returns:
        pd.dataframe -- start and stop times on each row
    """

    if machine_name == None :
        return data.loc[(data['cycle_start_time'] > begintime) & (data['cycle_stop_time'] < endtime)]
    machinename = machine_name.replace("_","")
    
    data_machine = data.loc[((data['machine_name'] == machine_name) | (data['machine_name'] == machinename))&(data['cycle_start_time'] > begintime) & (data['cycle_stop_time'] < endtime)&(data['cycle_stop_time']>data['cycle_start_time'])]
    
    return data_machine
#%%
def dic_date_to_dif_sec(dic_date_start={"hour": 11, "nano": 920000000, "minute": 47, "second": 6}, dic_date_stop={"hour": 14, "nano": 498000000, "minute": 26, "second": 20}):
    
    #convert to json
    try:
        dic_date_start = json.loads(dic_date_start)
        dic_date_stop = json.loads(dic_date_stop)
    except TypeError:
        return 0

    #convert to seconds
    sec1 = dic_date_start.get("hour")*3600 + dic_date_start.get("minute")*60 + dic_date_start.get("second") + float('0.'+str(dic_date_start.get("nano")))
    sec2 = dic_date_stop.get("hour")*3600 + dic_date_stop.get("minute")*60 + dic_date_stop.get("second") + float('0.'+str(dic_date_stop.get("nano")))

    #return the difference
    if sec1==0 or sec2==0:
        return 0
    return sec2-sec1
#%%
def dates_from_relative(df_times, column_date_reference, column_time_reference, column_time_target):
    """add column of date for a column where we only have the json format
    
    Arguments:
        df_times {dataframe} -- data for the start and stop times
        column_date_reference {str} -- name of the column
        column_time_reference {str} -- name of the column
        column_time_target {str} -- name of the column
    
    Returns:
        dataframe -- datframe original but with one more column showing the date for an event
    """
    
    #convert to datetime for the reference and target
    json_reference = json.loads(df_times[column_time_reference].to_json())
    json_target = json.loads(df_times[column_time_target].to_json())

    #compute target date
    date_target = []
    for i in range(len(df_times[column_date_reference])):
        delta_time = dic_date_to_dif_sec(dic_date_start=json_reference.get(str(i)),dic_date_stop=json_target.get(str(i)))
        date_target.append(df_times[column_date_reference].iloc[i] + pd.Timedelta(seconds=delta_time))

    #add the new collumn to dataframe
    df_times[(column_time_target + '_date')] = pd.Series(date_target,index=df_times.index)

    return df_times


#%%
def df_new_start_stop(start_stop_path='data\cycles_new_version.csv'):
    """transforms the file into a nice dataframe format
    
    Keyword Arguments:
        start_stop_path {str} -- relative path to the file (default: {'data\cycles_new_version.csv'})
    """
    #read csv
    df_times = pd.read_csv(start_stop_path,delimiter=',')

    #convert dates to df_times['timestamp'] to german UTC
    df_times['timestamp'] = pd.to_datetime(df_times['timestamp']).dt.tz_convert('Europe/Berlin')
    

    # shift the date
    #td = pd.Series([pd.Timedelta(hours=i) for i in np.ones(len(df_times['timestamp']))*(2)])
    #df_times['timestamp'] = df_times['timestamp'] + td
    #print(df_times.head)

    #compute start date in realtion to the column we think is the stop time
    df_times = dates_from_relative(df_times,column_date_reference='timestamp', column_time_reference='cores_put_from_shuttle_by_man', column_time_target='cycle_started')
    
    #compute ejection date from start
    reference = 'cycle_started'
    df_times = dates_from_relative(df_times,column_date_reference=(reference + '_date'), column_time_reference=reference, column_time_target='cores_ejected')

    #compute short_started from start
    df_times = dates_from_relative(df_times,column_date_reference=(reference + '_date'), column_time_reference=reference, column_time_target='shot_started')
    
    #compute gasing_started_date from start
    df_times = dates_from_relative(df_times,column_date_reference=(reference + '_date'), column_time_reference=reference, column_time_target='gasing_started')

    #define start and stop time
    df_times['cycle_start_time'] = df_times['gasing_started_date']
    df_times['cycle_stop_time'] = df_times['cores_ejected_date']

    #rename to machine_name
    df_times = df_times.rename(columns={'source':'machine_name'})

    #remove original stop time
    #df_times.drop(['timestamp'], axis=1, inplace=True)
    #print(df_times.head)
    return df_times

#%%
def diplay_time_delta_machines(df_start_stop_all):
    """plot the duration of the signals for different machines
    
    Arguments:
        df_start_stop_all {panda df} -- dataframe where we can get the machine number and the start and stop times
    """

    #initialize dic for storage data from different machines
    dict_time_delta_seconds_machines = {
        1:None,
        2:None,
        3:None,
        4:None,
        5:None,
        6:None
    }

    #fill the data into the dic
    for key,value in dict_time_delta_seconds_machines.items():
        machine_name = 'LHL_0'+str(key)
        df_start_stop_machine = getdf_start_stop_machine(df_start_stop_all,machine_name=machine_name)

        """
        dif_dates = []
        for index, row in df_start_stop_machine.iterrows():
            dif_dates = np.append(dif_dates,(row['cycle_stop_time']-row['cycle_start_time']).total_seconds())
        """

        dif_arrays = np.array(df_start_stop_machine['cycle_stop_time'])-np.array(df_start_stop_machine['cycle_start_time'])
        dif_seconds = [d.astype('timedelta64[ms]').astype('int')/1000.0 for d in dif_arrays]
        dif_seconds = [d for d in dif_seconds if d>0 and d<80]

        print(key)
        print(dif_seconds[:4])
        dict_time_delta_seconds_machines[key] = dif_seconds
    
    #plot histogram
    names = [('machine' + str(k)) for k,v in dict_time_delta_seconds_machines.items()]
    plt.hist([v for k,v in dict_time_delta_seconds_machines.items()],bins = int(150/15), normed=True, label=names)

    # Plot formatting
    plt.legend()
    plt.xlabel('Duration (sec)')
    plt.ylabel('Normalized machines')
    plt.title('Side-by-Side Histogram with Multiple Machines')
    plt.show()

    for k,v in dict_time_delta_seconds_machines.items():
        plt.hist(v,label=('machine' + str(k)),bins=100,density=True)
        plt.legend()
        plt.xlabel('Duration (sec)')
        plt.ylabel('Density')
        plt.show()
    
    return dict_time_delta_seconds_machines

#%%
if __name__=="__main__":
    
##%%
    df_new_start_stop()
##%%
    df_new_start_stop_all = df_new_start_stop()
    getdf_start_stop_machine(df_new_start_stop_all)

##%%
    df_start_stop_all = df_start_stop()
    #%%
    df_start_stop_6 = getdf_start_stop_machine(df_start_stop_all)
    df_start_stop_6

##%%
    df_new_start_stop_all = df_new_start_stop()
    print(df_new_start_stop_all.head)
    df_new_start_stop_machine = getdf_start_stop_machine(df_new_start_stop_all,machine_name='LHL_06')
    df_new_start_stop_machine


##%% using rows
    def deltas_rows(data, column_name='cores_put_from_shuttle_by_man'):
        deltas_sec_shot = []
        column = df_new_start_stop_machine[column_name]
        for row,next_row in zip(column, column.shift(-1)):
            delta_sec = dic_date_to_dif_sec(dic_date_start=row,dic_date_stop=next_row)
            if np.isnan(delta_sec):
                deltas_sec.append(0)
            if delta_sec>0:
                deltas_sec_shot.append(delta_sec)
            else:
                deltas_sec_shot.append(0)

        return deltas_sec_shot
##%%
    column_times = ['cycle_started','shot_started','gasing_started','corebox_opened','cores_ejected','cores_placed_on_shuttle','cores_put_from_shuttle_by_man']

##%%
    deltas_sec_shot = deltas_rows('cores_put_from_shuttle_by_man')
    plt.plot(deltas_sec_shot)
    print(np.median(deltas_sec_shot))
##%%
    deltas_sec_shots = [deltas_rows(df_new_start_stop_machine,column_name=c)  for c in column_times]
    [print(c[:5]) for c in deltas_sec_shots]
    plt.plot(deltas_sec_shots[0])
##%% Compute the deltas in the timestamp
    deltas_sec = []
    df_new_start_stop_machine['timestamp'] = df_new_start_stop_machine['timestamp'].shift(0)
    column = df_new_start_stop_machine['timestamp']
    for row,next_row in zip(column, column.shift(-1)):
        delta_sec = (next_row-row).total_seconds()
        #print(delta_sec)
        if np.isnan(delta_sec):
            deltas_sec.append(0)
        else:
            deltas_sec.append(delta_sec)
    plt.plot(deltas_sec)
    print(np.median(deltas_sec))
##%% plot the difference
    #deltas_dif = np.array(deltas_sec)-shift(np.array(deltas_sec_shot),shift=0,order=1)
    #plt.plot(deltas_dif)
    #print(np.median(abs(deltas_dif)))
    shis = [-2,-1,0,1,2]
    [print(np.median(np.abs(np.array(deltas_sec)-shift(np.array(s),shift=shi,order=1)))) for shi in shis for s in deltas_sec_shots ]
    #[plt.plot(np.array(deltas_sec)-shift(np.array(s),shift=-1,order=1)) for s in deltas_sec_shots[6]]
    #plt.show()
##%% 
    dif_array = (np.array(deltas_sec)-shift(np.array(deltas_sec_shots[6]),shift=-1,order=1))
    print(np.median(np.abs(dif_array)))
    plt.plot(dif_array)


##%%    
    #machine_name
    #cycle_start_time
    #cycle_stop_time

    [print(c) for c in df_start_stop_6.columns]
    #%%
    dif_dates = []
    for index, row in df_start_stop_6.iterrows():
        dif_dates = np.append(dif_dates,row['cycle_stop_time']-row['cycle_start_time'])

    print(dif_dates[:5])

    #%%
    plt.hist([d.total_seconds() for d in dif_dates],bins=100)



    #%%import df
    df_start_stop_all = df_start_stop()
    df_start_stop_all.head
    #%%
    df_new_start_stop_all = df_new_start_stop()
    df_new_start_stop_all.head
    #%%
    dif = np.array(df_new_start_stop_all['cycle_stop_time'])[:4]-np.array(df_new_start_stop_all['cycle_start_time'])[:4]

    [print(d.astype('timedelta64[s]').astype('int')) for d in dif]

    print(dif)
    #%% hists
    diplay_time_delta_machines(df_start_stop_all)
    #%%
    diplay_time_delta_machines(df_new_start_stop_all)

    #%%
    dict_time_delta_seconds_new_machines = diplay_time_delta_machines(df_new_start_stop_all)

##%%

    #%%
    with open ('dict_time_delta_seconds_new_machines.pickle','wb+') as fp:
        pickle.dump(dict_time_delta_seconds_new_machines,fp)


#%%
