#%% Libraries
import pandas as pd


#%% import inital table
start_stop_path = '/home/pier/Documents/stage/time_series_compare/dynamic-timewrapping/data/grafana_data_export.csv'
df_series = pd.read_csv(start_stop_path,delimiter=';')
df_series.head

#%% Print columns
# iterating the columns 
# Time
# P_Total, LHL_01
# P_Total, LHL_02
# P_Total, LHL_03
# P_Total, LHL_04
# P_Total, LHL_05
# P_Total, LHL_06
# P_Total, Mixer01
# P_Total, Mixer02
# S_Total,  LHL_01
# S_Total,  LHL_02
# S_Total,  LHL_03
# S_Total,  LHL_04
# S_Total,  LHL_05
# S_Total,  LHL_06
# S_Total,  Mixer01
# S_Total,  Mixer02
# P_Total, Climate Chamber
# S_Total, Climate Chamber

for col in df_series.columns: 
    print(col) 
 
#%% Convert time from str to datetime type
df_series['datetime'] = pd.to_datetime(df_series['Time'])
#%% drop time str
df_series.drop(['Time'], axis=1, inplace=True)
#%% Set time as index
df_series = df_series.set_index('datetime')

#%%
df_series['2019-07-29':'2019-07-29']['P_Total, LHL_06']
#%% function to return df_series with time as index 
def df_series(series_path = '/home/pier/Documents/stage/time_series_compare/dynamic-timewrapping/data/grafana_data_export.csv'):
    df_series = pd.read_csv(series_path,delimiter=';')
    
    #convert to datatime
    df_series['datetime'] = pd.to_datetime(df_series['Time'])
    df_series.drop(['Time'], axis=1, inplace=True)

    #time as index
    df_series = df_series.set_index('datetime')
    return df_series



#%% funcion to select the series for a certain machine
def getdf_series_machine(data,machine_name = 'LHL_06', begintime='2019-07-29',endtime='2019-07-30'):
    return data[begintime:endtime][str('P_Total, '+ machine_name)]

#%% Test function
df_series = df_series()
df_series.head
getdf_series_machine(df_series).head



#%%
