
#%%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import pickle
from scipy.ndimage.interpolation import shift
from importlib import reload

#%% import functions from files
from comparisons_cycle_time_and_datasets import df_new_start_stop, df_start_stop, getdf_start_stop_machine, dic_date_to_dif_sec
#%% Functions that don't wants to be loaded from other files

def df_series(series_path = 'data\grafana_data_export.csv'):
    """generare a dataframe from the csv with datatime series
    
    Keyword Arguments:
        series_path {str} -- path to csv file (default: {'/home/pier/Documents/stage/time_series_compare/dynamic-timewrapping/data/grafana_data_export.csv'})
    
    Returns:
        df_series -- pd.dataframe with time as index
    """
    df_series = pd.read_csv(series_path,delimiter=';')
    
    #convert to datatime
    df_series['datetime'] = pd.to_datetime(df_series['Time'])
    df_series.drop(['Time'], axis=1, inplace=True)

    #time as index
    #df_series = df_series.set_index('datetime')
    return df_series

def getdf_series_machine(data,machine_name = 'LHL_06', begintime='2019-07-29',endtime='2019-07-30'):
    """selects the data of a specific machine in the specific timerange of the df_series
    
    Arguments:
        data {pd.dataframe} -- like df_series
    
    Keyword Arguments:
        machine_name {str} -- change the number to select the wanted machine (default: {'LHL_06'})
        begintime {str} --  (default: {'2019-07-29'})
        endtime {str} --  (default: {'2019-07-30'})
    
    Returns:
        pd.Serie -- Looks like an array 
    """

    if machine_name == None :
        return data.loc[(data['datetime'] > begintime) & (data['datetime'] < endtime)]
    return data.loc[(data['datetime'] > begintime) & (data['datetime'] < endtime)&(data[str('P_Total, '+ machine_name)].notnull())][['datetime',str('P_Total, '+ machine_name)]]

#%% function to get array from index
def get_signal_from_index(data_series,data_start_stop, machine_name='LHL_06',index=0):
    """selects the iest cut into the dataseries
        2 prompt begin and end of the wanted index of the previous selection
        3 get the selection of the signal in the selection of the serie dataframe
        4 convert the selection into an array

    Arguments:
        data_series {df.serie} -- from a specific machine
        data_start_stop {df.dataframe} -- from a specific machine
    
    Keyword Arguments:
        index {int} -- the index of the cut (default: {0})
        machine_name
    
    Returns:
        array -- represent a signal
    """
    begin = data_start_stop.loc[data_start_stop.index[index]]['cycle_start_time']
    end = data_start_stop.loc[data_start_stop.index[index]]['cycle_stop_time']
    serie = getdf_series_machine(data_series,machine_name=machine_name,begintime=begin,endtime=end)
    array_signal = np.array(serie[str('P_Total, '+ machine_name)])
    return array_signal


#%% Save the signals in an array of signals
def get_signals_array(df_start_stop,df_series,begin_date='2019-07-29',end_date='2019-07-30',machine_name='LHL_06'):
    """cuts the df series into the individual signals and save them into an array
    
    Arguments:
        df_start_stop{pd.dataframa} -- all the start and stop of all the machines
        df_series {pd.Series} -- the dataset of the series
    
    Returns:
        array -- array of array
    """
    #Let's perform a firts selection on the dates: hope to rediuce the time complexity
    # first on the series
    df_series_machine = getdf_series_machine(df_series,machine_name=machine_name,begintime=begin_date,endtime=end_date)
    #print(df_series_machine.head)

    #secondly on the start stop
    df_start_stop_machine = getdf_start_stop_machine(df_start_stop,machine_name=machine_name,begintime=begin_date,endtime=end_date)
    
    #resolve the timestamp shift problem
    try:
        df_start_stop_machine['timestamp'] = df_start_stop_machine['timestamp'].shift(1)
    except KeyError:
        pass
    #print(df_start_stop_machine.head)

    #debug
    # using rows
    deltas_sec_shot = []
    column = df_start_stop_machine['cores_put_from_shuttle_by_man']
    for row,next_row in zip(column, column.shift(-1)):
        delta_sec = dic_date_to_dif_sec(dic_date_start=row,dic_date_stop=next_row)
        if delta_sec>0:
            deltas_sec_shot.append(delta_sec)
        else:
            deltas_sec_shot.append(0)

    #plt.plot(deltas_sec_shot)
    #print(np.median(deltas_sec_shot))

    # Compute the deltas in the timestamp
    deltas_sec = []
    column = df_start_stop_machine['timestamp']
    for row,next_row in zip(column, column.shift(-1)):
        delta_sec = (next_row-row).total_seconds()
        #print(delta_sec)
        if np.isnan(delta_sec):
            deltas_sec.append(0)
        else:
            deltas_sec.append(delta_sec)

    # plot the difference
    deltas_dif = np.array(deltas_sec)-shift(np.array(deltas_sec_shot),shift=0,order=1)
    plt.plot(deltas_dif)
    print(np.median(np.abs(deltas_dif)))



    signals = []
    #
    for i in range(len(df_start_stop_machine)-1):
        signal = get_signal_from_index(df_series_machine,df_start_stop_machine,machine_name=machine_name,index=i)
        signals.append(signal)
    return signals

#%% import all series
df_series_all= df_series()
df_series_all

#%% test serie selection
machine6 = getdf_series_machine(df_series_all,machine_name='LHL_01')
machine6
#%% import start stop new
df_new_start_stop_all = df_new_start_stop()
df_new_start_stop_all

#%%selection in start stop new
df_new_start_stop_machine = getdf_start_stop_machine(df_new_start_stop_all,machine_name='LHL_06')
df_new_start_stop_machine['timestamp'] = df_new_start_stop_machine['timestamp'].shift(1)
df_new_start_stop_machine
#%%selection in the old
df_start_stop_all=df_start_stop()
machien6_start_stop = getdf_start_stop_machine(df_start_stop_all,machine_name='LHL_06')
machien6_start_stop
#%% Test signal from index
df_start_stop_all=df_start_stop()
machine6 = getdf_series_machine(df_series_all,machine_name='LHL_06')
machien6_start_stop = getdf_start_stop_machine(df_start_stop_all,machine_name='LHL_06')
signal = get_signal_from_index(machine6,machien6_start_stop,machine_name='LHL_06',index=3)
plt.plot(signal)
plt.show()
#%%
df_series_machine = getdf_series_machine(df_series_all,machine_name='LHL_06',begintime='2019-07-29',endtime='2019-07-30')
print(df_series_machine.head)

#%%
print(df_series_all.head)
print(df_start_stop_all.head)

#%% test for the newdata
df_new_start_stop_all = df_new_start_stop()
#%%
signal_array = get_signals_array(df_new_start_stop_all,df_series_all,machine_name='LHL_04')
#print(len(signal_array))
#plt.hist([len(s) for s in signal_array])
#%%
plt.plot(signal_array[0])
plt.show()

#%%
plt.plot(signal_array[25])
plt.show()

#%% Works
for i in range(len(signal_array)-1):
    plt.plot(signal_array[i][:60])
plt.show()
#%% Works
off=40

for i in range(0+off,10+off):
    #print(i)
    plt.plot(signal_array[i],label=str(i))
plt.legend()
plt.show()

#%% save in pickle
with open ('signals.pickle','wb+') as fp:
    pickle.dump(signal_array,fp)

#%%
with open('signals.pickle','rb') as fp:
    test = pickle.load(fp)

plt.plot(test[0])
plt.show()