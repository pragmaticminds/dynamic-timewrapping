
#%% Libraries
import numpy as np
from scipy.optimize import minimize, shgo
from scipy import signal
from scipy.ndimage.interpolation import shift
import pickle
from matplotlib import pyplot as plt
#%%
import time
#%%
from slide_two_arrays import slide, slide_and_equalize 
#%%Import shift function
def equalize_array_size_add_median(array1,array2):
    """reduce the size of an array to make them equal size
    
    Arguments:
        array1 {array like} -- 1
        array2 {array like} -- 2
    
    Returns:
        two arrays -- the biggest array has been reduce in size
    """
    #reduce the size of a sample to make them equal size
    len1, len2 = len(array1), len(array2)
    dif_length = len1-len2
    if dif_length<0:
        med = np.median(array1)
        array1 = np.append(np.append(np.ones(int(np.floor(dif_length/2)))*med,array1),np.ones(int(np.ceil(dif_length/2)))*med)
    elif dif_length>0:
        med = np.median(array2)
        array2 = np.append(np.append(np.ones(int(np.floor(dif_length/2)))*med,array2),np.ones(int(np.ceil(dif_length/2)))*med)
    return array1,array2, dif_length

def equalize_array_size(array1,array2):
    '''
    reduce the size of one sample to make them equal size. 
    The sides of the biggest signal are truncated

    Args:
        array1 (1d array/list): signal for example the reference
        array2 (1d array/list): signal for example the target

    Returns:
        array1 (1d array/list): middle of the signal if truncated
        array2 (1d array/list): middle of the initial signal if there is a size difference between the array 1 and 2
        dif_length (int): size diffence between the two original arrays 
    '''
    len1, len2 = len(array1), len(array2)
    dif_length = len1-len2
    if dif_length<0:
        array2 = array2[int(np.floor(-dif_length/2)):len2-int(np.ceil(-dif_length/2))]
    elif dif_length>0:
        array1 = array1[int(np.floor(dif_length/2)):len1-int(np.ceil(dif_length/2))]
    return array1,array2, dif_length
#%%
def chisqr_align(reference, target, roi=None, order=1, init=0, bound=1,method='L-BFGS-B',tol=0.2):
    '''
    Align a target signal to a reference signal within a region of interest (ROI)
    by minimizing the chi-squared between the two signals. Depending on the shape
    of your signals providing a highly constrained prior is necessary when using a
    gradient based optimization technique in order to avoid local solutions.

    Args:
        reference (1d array/list): signal that won't be shifted
        target (1d array/list): signal to be shifted to reference
        roi (tuple): region of interest to compute chi-squared
        order (int): order of spline interpolation for shifting target signal
        init (int):  initial guess to offset between the two signals
        bound (int): symmetric bounds for constraining the shift search around initial guess

    Returns:
        shift (float): offset between target and reference signal 
    
    Todo:
        * include uncertainties on spectra
        * update chi-squared metric for uncertainties
        * include loss function on chi-sqr

    '''
    if roi==None: roi = [0,len(reference)-1] 
  
    # convert to int to avoid indexing issues
    ROI = slice(int(roi[0]), int(roi[1]), 1)

    # normalize ref within ROI
    #reference = (reference/np.mean(reference[ROI]))[ROI]

    # define objective function: returns the array to be minimized
    def fcn2min(s,reference=reference, target=target):
        reference, target = slide_and_equalize(reference, target,shift_step=s,order=order)
        return np.sum((reference - target)**2 )/len(reference)**2 

    # set up bounds for pos/neg shifts
    minb = min( [(init-bound),(init+bound)] )
    maxb = max( [(init-bound),(init+bound)] )

    minimizer_kwargs = {"tol":tol}
    ret = shgo(fcn2min,bounds= [ (minb,maxb) ],iters=5,options={"f_tol": tol},minimizer_kwargs=minimizer_kwargs)

    return {'shift':ret.x[0], 'fun':ret.fun}
#%% Import signals
with open('signals.pickle','rb') as fp:
    signals = pickle.load(fp)

plt.plot(signals[0])
#plt.show()

#%%
sig1 = signals[15][53:]
sig2 = signals[11][53:]
plt.plot(sig1)
plt.plot(sig2)
#plt.show()

#%%chi squared alignment at native resolution
timeloc = time.time()
#shift12,fun = chisqr_align(sig1,sig2, bound=20,order=1,method='nelder-mead',tol=0.3)

chisqr = chisqr_align(sig1,sig2, bound=20,order=1,method='nelder-mead',tol=0.3)
shift12 = chisqr.get('shift')
print('time' + str((time.time()-timeloc)*1000))
print('shift is' + str(shift12))
print('R² is ' + str(chisqr.get('fun')))
plt.plot(sig1)
plt.plot(sig2)
#sig3 = np.append(np.ones(shift12)*np.median(sig2),sig2)
#plt.plot(sig3,color='red')
plt.plot(shift(sig2,shift12,mode='nearest'),ls='--',label='aligned data') 

#%%

NPTS = 100
SHIFTVAL = 4
NOISE = 1e-2 # can perturb offset retrieval from true
#print('true signal offset:',SHIFTVAL)

# generate some noisy data and simulate a shift
og = signal.gaussian(NPTS, std=4) + np.random.normal(1,NOISE,NPTS)
shifted = shift( signal.gaussian(NPTS, std=4) ,SHIFTVAL) + np.random.normal(1,NOISE,NPTS)
#plt.plot(og,label='original data')
#plt.plot(shifted,label='shifted data')
#%%
chisqr = chisqr_align(og, shifted,roi=[1,90],bound=20,order=2)
print('R² is ' + str(chisqr.get('fun')))
#print('chi square alignment',s)
#%%
# make some diagnostic plots
plt.plot(og,label='original data')
plt.plot(shifted,label='shifted data')
plt.plot(shift(shifted,chisqr.get('shift'),mode='nearest'),ls='--',label='aligned data') 
plt.legend(loc='best')
#plt.show()

#%%
